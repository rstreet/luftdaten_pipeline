## -------------------------------------------------------------------------
## R Script for processing raw data of sensors: 
## 
## - load RDS files with raw data from prev saved step
## - handling outliers
## - merging dht/sds observations into 1 node obersvation 
## - check active nodes during the period
## - check for consistent off readings individual nodes 
## - pm10, pm2.5 correction for high humidity levels
## - daylight savings correction 
## - hourly aggregation   
## 
## - saving processed data in RDS file for analysis
##
## Author : R. Straatemeier
## Last update: july 2020
## -------------------------------------------------------------------------

library (here)
library (tidyverse)
library (lubridate)
library (ggplot2)

# settings

#input
RDS_DHT_SENSOR_DATAFILE_IN = "arnhemcentrum_lda_dht_202001_202006_raw.rds"
RDS_SDS_SENSOR_DATAFILE_IN = "arnhemcentrum_lda_sds_202001_202006_raw.rds"

#output
RDS_SENSOR_DATAFILE_OUT = "arnhemcentrum_202001_202006_processed.rds"

#sensor location
SENSOR_FILENAME ="list_sensors.csv"

# set current script folder as working directory
setwd(here())

# --- helpers
outlierCheck <- function(dt, var) {
  var_name <- eval(substitute(var),eval(dt))
  na1 <- sum(is.na(var_name))
  m1 <- mean(var_name, na.rm = T)
  par(mfrow=c(2, 2), oma=c(0,0,3,0))
  boxplot(var_name, main="With outliers")
  hist(var_name, main="With outliers", xlab=NA, ylab=NA)
  outlier <- boxplot.stats(var_name)$out
  cutoff <-  boxplot.stats(var_name)$stats
  mo <- mean(outlier)
  var_name <- ifelse(var_name %in% outlier, NA, var_name)
  boxplot(var_name, main="Without outliers")
  hist(var_name, main="Without outliers", xlab=NA, ylab=NA)
  title("Outlier Check", outer=TRUE)
  na2 <- sum(is.na(var_name))
  cat("Outliers identified:", na2 - na1, "\n")
  cat("Propotion (%) of outliers:", round((na2 - na1) / sum(!is.na(var_name))*100, 1), "\n")
  cat("Mean of the outliers:", round(mo, 2), "\n")
  m2 <- mean(var_name, na.rm = T)
  cat("Mean without removing outliers:", round(m1, 2), "\n")
  cat("Mean if we remove outliers:", round(m2, 2), "\n")
  cat("Suggested cutoff low: ", cutoff[1],"cutoff high: ", cutoff[5], "\n")
  par(mfrow=c(1,1))
}



# read sensor file
sensor_list <- read.csv(SENSOR_FILENAME, sep=";", stringsAsFactors = F)


# read raw sensorvalues
dht_raw <- readRDS(RDS_DHT_SENSOR_DATAFILE_IN)
sds_raw <- readRDS(RDS_SDS_SENSOR_DATAFILE_IN)

#
#--------------------------------------------
# outlier analysis Temperature measurements
#--------------------------------------------

summary (dht_raw$temperature)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# -2.50    9.80   14.30   15.26   20.10  563.9

outlierCheck(dht_raw, temperature)

# Outliers identified: 7609 
# Propotion (%) of outliers: 0.4 
# Mean of the outliers: 42.58 
# Mean without removing outliers: 15.26 
# Mean if we remove outliers: 15.15 
# Suggested cutoff low:  -2.5 cutoff high:  35.5 

# -> Conservative temperature cutoff around 40

temp_cutoff <- 40

# create suspectlist
suspect_sensorsID<- dht_raw %>% 
  filter(temperature>temp_cutoff) %>%
  group_by(sensor_id) %>%
  summarise(cnt_outlier_obs=n()) %>%
  mutate(type_obs='temp')

# histogram of all temp observations
dht_raw %>%
  ggplot(aes(temperature)) + 
  geom_histogram(aes(fill=..count..), 
                 breaks=seq(-5, temp_cutoff, by = 2),
                 col="gray") + 
  labs(title="Histogram for DHT Temp observations") +
  labs(x="Celcius", y="Count")

# delete observations
dht_raw <- dht_raw %>%
  mutate(temperature=ifelse(temperature > temp_cutoff, NA, temperature))

summary (dht_raw$temperature)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -2.50    9.80   14.30   15.22   20.10   40.00    1770 


#--------------------------------------------
# outlier analysis Humidity measurements
#--------------------------------------------

summary (dht_raw$humidity)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   15.90   43.60   58.70   59.24   75.00 1126.40      13

outlierCheck(dht_raw, humidity)

# Outliers identified: 62 
# Propotion (%) of outliers: 0 
# Mean of the outliers: 1041.75 
# Mean without removing outliers: 59.24 
# Mean if we remove outliers: 59.21 
# Suggested cutoff low:  15.9 cutoff high:  99.9 

# -> Logical cutoff 100

hum_cutoff <- 100

# add to suspect list
suspect_sensorsID<- rbind (suspect_sensorsID, dht_raw %>% 
  filter(humidity>hum_cutoff) %>%
  group_by(sensor_id) %>%
  summarise(cnt_outlier_obs=n()) %>%
  mutate(type_obs='hum'))


# histogram of all humidity observations
dht_raw %>%
  ggplot(aes(humidity)) + 
  geom_histogram(aes(fill=..count..), 
                 breaks=seq(0, hum_cutoff, by = 2),
                 col="gray") + 
  labs(title="Histogram for DHT Humidity observations") +
  labs(x="% hum", y="Count")

# delete observations
dht_raw <- dht_raw %>%
  mutate(humidity=ifelse(humidity > hum_cutoff, NA, humidity))

summary (dht_raw$humidity)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   15.90   43.60   58.70   59.21   75.00   99.90      75 

#--------------------------------------------
# outlier analysis PM10 measurements
#--------------------------------------------

summary (sds_raw$P1)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0.00    3.72    6.93   31.20   12.32 1999.90

outlierCheck(sds_raw, P1)

# Outliers identified: 135552 
# Propotion (%) of outliers: 7.7 
# Mean of the outliers: 334.98 
# Mean without removing outliers: 31.2 
# Mean if we remove outliers: 7.74 
# Suggested cutoff low:  0 cutoff high:  25.2 

# -> Logical cutoff 500

pm10_cutoff <- 500

# add to suspect list
suspect_sensorsID <- rbind (suspect_sensorsID, sds_raw %>% 
         filter(P1 > pm10_cutoff) %>%
         group_by(sensor_id) %>%
         summarise(cnt_outlier_obs=n()) %>%
         mutate(type_obs='pm10'))

# histogram of all humidity observations
sds_raw %>%
  ggplot(aes(P1)) + 
  geom_histogram(aes(fill=..count..), 
                 breaks=seq(0, 50, by = 2),
                 col="gray") + 
  labs(title="Histogram for SDS PM10 observations") +
  labs(x="mugr/m3", y="Count")

# delete observations
sds_raw <- sds_raw %>%
  mutate(P1=ifelse(P1 > pm10_cutoff, NA, P1))

summary (sds_raw$P1)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
# 0.00    3.70    6.85   10.39   12.05  499.62   21953 


#--------------------------------------------
# outlier analysis PM2.5 measurements
#--------------------------------------------

summary (sds_raw$P2)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#    0.00    1.30    2.65   14.68    5.90  999.90       8 

outlierCheck(sds_raw, P2)

# Outliers identified: 170329 
# Propotion (%) of outliers: 9.9 
# Mean of the outliers: 129.47 
# Mean without removing outliers: 14.68 
# Mean if we remove outliers: 3.32 
# Suggested cutoff low:  0 cutoff high:  12.8 

# -> Logical cutoff 500

pm25_cutoff <- 500

# add to suspect list
suspect_sensorsID <- rbind (suspect_sensorsID, sds_raw %>% 
                              filter(P2 > pm25_cutoff) %>%
                              group_by(sensor_id) %>%
                              summarise(cnt_outlier_obs=n()) %>%
                              mutate(type_obs='pm25'))

#histogram of all humidity observations
sds_raw %>%
  ggplot(aes(P2)) + 
  geom_histogram(aes(fill=..count..), 
                 breaks=seq(0, 50, by = 2),
                 col="gray") + 
  labs(title="Histogram for SDS PM2.5 observations") +
  labs(x="mugr/m3", y="Count")

# delete observations
sds_raw <- sds_raw %>%
  mutate(P2=ifelse(P2 > pm25_cutoff, NA, P2))

summary (sds_raw$P2)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   0.000   1.300   2.620   5.543   5.720 500.000   19103 

#----------------------------------------------
# aggregate dht/sds measurement per node
# - assumption: node-> dht sensorid = sds sensorID+1
#---------------------------------------------

# timestamp synchronisation dht/sds measurements on 1 min scale
dht_raw <- dht_raw %>%
  mutate(timestamp_synch= lubridate::round_date(timestamp, "1 minutes") )

sds_raw <- sds_raw %>%
  mutate(timestamp_synch= lubridate::round_date(timestamp, "1 minutes") ) %>%
  mutate(sensor_id_dht =sensor_id+1)

# combine measurements 
node_obs <- sds_raw %>%
  left_join(dht_raw, by=c('sensor_id_dht'= 'sensor_id', 'timestamp_synch'), suffix = c("_sds", "_dht"))

# double check distributions of measurements old and new
# and make sure there is no massive shift by above operation

# temp org
summary (dht_raw$temperature)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -2.50    9.80   14.30   15.22   20.10   40.00    1770 

# temp new
summary (node_obs$temperature)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -1.30    9.90   14.40   15.27   20.10   40.00  197542 

"-> ok"

# humidity org
summary (dht_raw$humidity)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   15.90   43.60   58.70   59.21   75.00   99.90      75 

# humidity new
summary (node_obs$humidity)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   15.90   43.80   58.80   59.32   75.00   99.90  195877 

"-> ok"

# all seems to look ok, slight move, new NA's but seems ok, 
# explanation: that some dht measurements have no corresponding sds measurements and vice versa
# no need to check sds measurements because of the left join operation

#---------------------------------------------
# check amount of active sensors during period
#---------------------------------------------

# low numbers can have huge impact on averages/analysis so
# worth exploring how the amount of sensors is evolving

# only incorporating sensor averages with enough readings during 1 day
# -> 24 (hours) * 20 (3 min intervals) ~ 500 readings 
# so say a minimum of 100 reading/day 

min_obs_per_node_per_day = 100

daily_node_avg <- node_obs %>%
  mutate(date=as.Date(timestamp_synch)) %>%
  group_by(sensor_id, date) %>%
  summarise(cnt= n(),
            pm10 = median(P1, na.rm=T), 
            pm25 = median(P2, na.rm=T), 
            temp = median(temperature, na.rm=T), 
            hum = median(humidity, na.rm=T) ) %>%
  filter (cnt > min_obs_per_node_per_day )
  

sma07 <- rollify(mean, window = 7)

#plot total valid nodes in period
daily_node_avg %>%
  group_by(date) %>%
  summarise(count_active_nodes=n()) %>%
  mutate (weekly_avg=sma07(count_active_nodes)) %>%
  ggplot(aes(x=date)) +
  geom_line(aes(y = count_active_nodes), colour="grey") +
  geom_line(aes(y = weekly_avg), colour="blue", size=1) +
  scale_x_date(date_breaks = "1 month",
               date_labels = "%b-%y") +
  labs(x = "Date", y = "Active Nodes") +
  ggtitle( label = "Active Node Count for Arnhem Centrum",
           subtitle= paste0("Min threshold: ",min_obs_per_node_per_day, " active readings/day") ) 

# -> In first months not a lot of nodes but growing in numbers, this will indicate more
# volatile average readings for the first couple of months, from march on it seems somewhat more stable

#---------------------------------------------
# check consistency readings individual nodes
#---------------------------------------------


# plot and check PM10, PM2.5 readings for all individual sensors 
# to identify if there are some problems with sensors

daily_node_avg %>%
  inner_join(sensor_list, by="sensor_id") %>%
  mutate(node = paste0(area," (id:",sensor_id,")")) %>%
  group_by(node, date, area) %>%
  ggplot(aes(x=date)) +
  geom_line(aes(y = pm10), colour="blue") +
  geom_line(aes(y = pm25), colour="red") +
  scale_x_date(date_breaks = "1 month",
               date_labels = "%b") +
  facet_wrap(~node, scale='free') +
  ggtitle( label = "Check PM10/PM2.5 sensors readings Arnhem Centrum",
           subtitle= paste0("Min threshold: ",min_obs_per_node_per_day, " active readings/day") ) 

# we see 1 sensors id 39060, 43550, 47335 giving clear false readings in comparison to other sensors
# eyeballing we can have a much tighter cutoff for pm outliers, perv 500, now 100 will be ok

pm_new_cutoff <- 100

# delete observations
node_obs <- node_obs %>%
  mutate(P1=ifelse(P1 > pm_new_cutoff, NA, P1),
         P2=ifelse(P2 > pm_new_cutoff, NA, P2))

# also exclude readings for bad nodes id 39060, 43550, 47335
node_obs <- node_obs %>%
  filter (!sensor_id %in% c(39060,43550, 47335))

#------------------------------------------------------
# Influence of high humidity levels on sds measurements
# -----------------------------------------------------

# Datasheet sds011:
#   https://cdn-reichelt.de/documents/datenblatt/X200/SDS011-DATASHEET.pdf
# -> Max humidity working env 70%
# 
# The influence of humidity on the performance of a low-cost air
# particle mass sensor and the effect of atmospheric fog
# https://www.atmos-meas-tech.net/11/4883/2018/amt-11-4883-2018.pdf
# -> Above 75% rel. humidity readings of the sensor start to fluctuate too much.

hum_max_threshold <- 75


# a peek in our observations, is there a clear correlation 


# whole range humidity observations
node_obs %>%
  filter(!is.na(humidity)) %>%
  mutate(hum_quantile= ntile(humidity,5)) %>%
  group_by(hum_quantile) %>%
  summarise(P10=mean(P1, na.rm=T), P25=mean(P2, na.rm=T)) %>%
  gather("sds_measurements", "avg_pollution", -hum_quantile) %>%
  ggplot(aes(x=hum_quantile, y=avg_pollution, fill=sds_measurements) ) +
  geom_bar(stat='identity', position='dodge') 

# -> seems to be a clear positive relation between humidity and pollution 

# now inspect correlation on for the higher values humidity
node_obs %>%
  filter(!is.na(humidity) & humidity> hum_max_threshold) %>%
  mutate(high_humidity_quantile= ntile(humidity,5)) %>%
  group_by(high_humidity_quantile) %>%
  summarise(P10=mean(P1, na.rm=T), P25=mean(P2, na.rm=T)) %>%
  gather("sds_measurements", "avg_pollution", -high_humidity_quantile) %>%
  ggplot(aes(x=high_humidity_quantile, y=avg_pollution, fill=sds_measurements) ) +
  geom_bar(stat='identity', position='dodge') 

# -> also the higher regions a positive relationship, nearly linear 

# so will do a linear correction factor if humidity above hum_max_threshold


node_obs <- node_obs %>%
  mutate(P1=ifelse(humidity >hum_max_threshold, NA, P1),
         P2=ifelse(humidity >hum_max_threshold, NA, P2))

# check result correction
node_obs %>%
  filter(!is.na(humidity) & humidity> hum_max_threshold) %>%
  mutate(high_humidity_quantile= ntile(humidity,5)) %>%
  group_by(high_humidity_quantile) %>%
  summarise(P10=mean(P1, na.rm=T), P25=mean(P2, na.rm=T)) %>%
  gather("sds_measurements", "avg_pollution", -high_humidity_quantile) %>%
  ggplot(aes(x=high_humidity_quantile, y=avg_pollution, fill=sds_measurements) ) +
  geom_bar(stat='identity', position='dodge') 



#-------------------------------------------------------------
# create avg hourly observations  and  add seasonal components
# -------------------------------------------------------------

# daylight savings correction for comparable hourly analysis
node_obs <- node_obs %>%
  mutate(timestamp_synch_dlts = as.POSIXct(format(timestamp_synch, tz="Europe/Berlin", usetz=TRUE)))


# hourly aggregation based on median observation
node_obs_avgh <- node_obs %>%
  mutate(daynr =day(timestamp_synch_dlts),
         day=weekdays(timestamp_synch_dlts), 
         week=week(timestamp_synch_dlts), 
         month=month(timestamp_synch_dlts), 
         quarter=quarter(timestamp_synch_dlts),
         year=year(timestamp_synch_dlts),
         hour=hour(timestamp_synch_dlts)) %>%
  group_by(sensor_id, year, quarter, month, day, daynr, hour) %>%
  summarise(pm10 = median(P1, na.rm=T), 
            pm25 = median(P2, na.rm=T), 
            temp = median(temperature, na.rm=T), 
            hum = median(humidity, na.rm=T) )

# check if all cleaning / merging / correction / aggregation operations did not result in  
# a massive shift in distribution compared to the raw data

summary (dht_raw$temperature)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -2.50    9.80   14.30   15.22   20.10   40.00    1770 

summary (node_obs_avgh$temp)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -1.10    9.85   14.30   15.19   20.00   40.00    6924 

"-> ok"

summary (dht_raw$humidity)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   15.90   43.60   58.70   59.21   75.00   99.90      75 

summary (node_obs_avgh$hum)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   16.35   43.35   57.90   58.55   73.70   99.90    6894 

"-> ok"

summary (sds_raw$P1)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#    0.00    3.70    6.85   10.39   12.05  499.62   21953
   
summary (node_obs_avgh$pm10)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   0.022   2.700   4.810   6.681   8.350  98.930    6938 

# -> 27% drop in median drop is quite substantial, due to median hourly aggregation ?"

summary (node_obs$P1)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#    0.00    2.67    4.90    6.81    8.63   99.97  195538 

# -> nope not due to hourly aggregation, os it is the high humility val correction, and individual sensor correction 
# if we had discarded the observations with high humidity these drops would be even bigger.

summary (sds_raw$P2)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   0.000   1.300   2.620   5.543   5.720 500.000   19103 

summary (node_obs_avgh$pm25)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   0.000   0.910   1.885   3.158   3.720  97.695    6902 

#-> 29% drop see pm10"



#-----------------------------------------------
# Save cleaned, corrected and aggregated data 
# in R object data file for further analysis 
#-----------------------------------------------

saveRDS(node_obs_avgh, RDS_SENSOR_DATAFILE_OUT)
