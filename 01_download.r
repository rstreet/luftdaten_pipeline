## -------------------------------------------------------------------------
## R Script for downloading data .csv files from luftdaten archives to local 
##
## - input  : 'list_sensors.csv' is an input file containing the luftdaten 
##            sensorid's you wish to download data
##
## - output : in output folder 'csv' for each day and each sensor 
##            if available a downloaded .csv file
##
## Author : R. Straatemeier
## Last update: july 2020
## -------------------------------------------------------------------------


library(downloader)
library(here)

#settings

START_DATE = "2020-1-1"
END_DATE = "2020-6-30"

CSV_DIR = "csv"
URL_ARCHIVE = "https://archive.sensor.community"
SENSOR_FILENAME ="list_sensors.csv"

# set current script folder as working directory
setwd(here())

# create days download period
selection_period_days <- seq(as.Date(START_DATE), as.Date(END_DATE), by="days")

# get sensorid's for downloading
sensor_list <- read.csv(SENSOR_FILENAME, sep=";", stringsAsFactors = F)

# init progress indication vars
max_cnt <- length(selection_period_days) * nrow(sensor_list)
cnt<-0

# download and save files for all days in period of interest and for all selected sensorid's
for (i in 1:length(selection_period_days)) { 
  
  for (j in 1:nrow(sensor_list)) {
    
    download_fname <- paste0( URL_ARCHIVE, "/"
                              , selection_period_days[[i]],"/"
                              , selection_period_days[[i]],"_"
                              , sensor_list$type[[j]],"_sensor_"
                              , sensor_list$sensor_id[[j]],".csv")
    
    export_fname <- paste0( CSV_DIR,"//",selection_period_days[[i]],"_", sensor_list$type[[j]],"_", sensor_list$sensor_id[[j]],".csv")
    
    try_download <- suppressWarnings(
                        try(download.file(download_fname, destfile=export_fname, quiet = T), silent = T)
                    )
    
    cnt <- cnt + 1
    
    print(paste0("Progress: ", round(cnt*100/max_cnt, digits=1)," %"))
    
    if(is(try_download,"try-error")){
      print(paste0("ERROR: ", download_fname ))
      Sys.sleep(1)
    } else{
      print(paste0("Downloaded: ", download_fname, " -> ", export_fname))
      Sys.sleep(3) # wait x seconds, so we don't use all server bandwidth
    }
    
    
  }
}

print ("READY")
