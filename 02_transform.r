## -------------------------------------------------------------------------
## R Script for transforming data from .csv files into R object files   
##
## Author : R. Straatemeier
## Last update: july 2020
## -------------------------------------------------------------------------

library (here)
library(tidyverse)

# settings
CSV_DIR = "csv"
RDS_DHT_SENSOR_DATAFILE = "arnhemcentrum_lda_dht_202001_202006_raw.rds"
RDS_SDS_SENSOR_DATAFILE = "arnhemcentrum_lda_sds_202001_202006_raw.rds"

# set current script folder as working directory
setwd(here::here())

# read all filenames to be processed
file_names_dht <- dir(path=CSV_DIR, pattern="*dht.*\\.csv") 
file_names_sds <- dir(path=CSV_DIR, pattern="*sds.*\\.csv") 

# read data within files into 1 big Tibble
readDataSensorFiles <- function (files) {
  return(read_delim(files, delim=";", col_types = cols()))
}

# dht sensors temp and humidity
raw_sensor_data_dht <-do.call(rbind,lapply(paste0(CSV_DIR,"\\", file_names_dht),readDataSensorFiles))

# save raw data in R object format
saveRDS(raw_sensor_data_dht, RDS_DHT_SENSOR_DATAFILE)

# sds sensors PM10, PM2.5
raw_sensor_data_sds <-do.call(rbind,lapply(paste0(DATA_DIR,"\\", file_names_sds),readDataSensorFiles))



# save raw data in R object format
saveRDS(raw_sensor_data_sds, RDS_SDS_SENSOR_DATAFILE)

