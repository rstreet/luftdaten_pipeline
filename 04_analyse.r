## -------------------------------------------------------------------------
## R Script for analysis of the data of sensors: 
## 
## - load RDS files with cleaned data from prev saved step
## - 
##
## Author : R. Straatemeier
## Last update: july 2020
## -------------------------------------------------------------------------

library (here)
library (tidyverse)
library (lubridate)
library (tibbletime)
library (ggplot2)

# dates lockdown
LOCKDOWN_FASEN =
  data.frame( event_date=as.Date(c("2020-03-23", "2020-06-01")), 
              event=c("start lockdown", "verdere versoepeling"))

# settings
RDS_SENSOR_DATAFILE_CLEAN = "arnhemcentrum_202001_202006_processed.rds"

#sensor location
SENSOR_FILENAME ="list_sensors.csv"

# set current script folder as working directory
setwd(here::here())


# load data
all_hourly_obs <- readRDS(RDS_SENSOR_DATAFILE_CLEAN)

# read sensor file
sensor_list <- read.csv(SENSOR_FILENAME, sep=";", stringsAsFactors = F)

# just plot average PM10, PM2.5,
min_obs_per_node_per_day = 6 #we have aggrageted data so say 6 hours 

daily_node_avg <- all_hourly_obs %>%
  mutate(date=as.Date(paste0(year,"-", month,"-",daynr)) ) %>% 
  group_by(sensor_id, date) %>%
  summarise(cnt= n(),
            avg_pm10 = median(pm10, na.rm=T), 
            avg_pm25 = median(pm25, na.rm=T), 
            avg_temp = median(temp, na.rm=T), 
            avg_hum = median(hum, na.rm=T) ) %>%
  filter (cnt > min_obs_per_node_per_day )



#overall plot
daily_node_avg %>%
  select (sensor_id, date, avg_pm10, avg_pm25, avg_hum, avg_temp ) %>%
  group_by(date) %>%
  summarise(avg_tot_pm10 = median(avg_pm10, na.rm=T),
            avg_tot_pm25 = median(avg_pm25, na.rm=T),
            avg_tot_hum  = median(avg_hum, na.rm=T),
            avg_tot_temp = median(avg_temp, na.rm=T),) %>%
  ggplot(aes(x=date)) +
  geom_smooth(aes(y = avg_tot_pm10, color="pm10 avg"), method="auto", se=TRUE, fullrange=FALSE, level=0.95) +
  geom_smooth(aes(y = avg_tot_pm25, color="pm25 avg"), method="auto", se=TRUE, fullrange=FALSE, level=0.95) +
  geom_line(aes(y = avg_tot_pm10, color="pm10"), size=.4,  alpha=.6) +
  geom_line(aes(y = avg_tot_pm25, color="pm25"), size=.4,  alpha=.8) +
  geom_vline(data=LOCKDOWN_FASEN, mapping=aes(xintercept=event_date), color="grey")  +
  geom_text(data=LOCKDOWN_FASEN, mapping=aes(x=event_date, y=15, label=event), size=4, angle=90, vjust=-0.4, hjust=0) +
  scale_x_date(date_breaks = "1 month",
               date_labels = "%b-%y") +
  scale_color_manual(values = c(
    "pm10" = "blue",
    "pm25" = "red",
    "pm10 avg" = "blue",
    "pm25 avg" = "red")) +
  labs(color = 'Type') +
  labs(x = "Date", y = "mug/m3") +
  ggtitle( label = "Arnhem Centrum Air Quality (corrected data)",
           subtitle= "avg daily values, first half of 2020") +
  theme_bw()

# breakdown per area
daily_node_avg %>%
  left_join(sensor_list, by="sensor_id") %>%
  select (sensor_id, area, date, avg_pm10, avg_pm25 ) %>%
  na.omit() %>%
  group_by(date, area) %>%
  summarise(avg_tot_pm10 = mean(avg_pm10, na.rm=T),
            avg_tot_pm25 = mean(avg_pm25, na.rm=T)) %>%
  ggplot(aes(x=date)) +
  geom_line(aes(y = avg_tot_pm10, color="pm10")) +
  geom_line(aes(y = avg_tot_pm25, color="pm25")) +
  geom_vline(data=LOCKDOWN_FASEN, mapping=aes(xintercept=event_date), color="grey")  +
 # scale_x_date(date_breaks = "1 month",
  #             date_labels = "%b-%y") +
  scale_color_manual(values = c(
    "pm10" = "blue",
    "pm25" = "red")) +
  labs(color = 'Type') +
  labs(x = "Date", y = "mug/m3") +
  ggtitle( label = "Arnhem Centrum Air Quality first half 2020 (corrected data)",
           subtitle="Breakdown per area") +
  facet_wrap(~area) +
  theme_bw()

# from eyeballing there all of the above there 
# is no obvious effect from the lockdown, no need for statistical tests

# seasonal analysis

# maybe we have to look at an hourly analysis
# and compare avg hourly pollution in lockdown vs out of lockdown
# there might be a visible effect around working hours

# hourly breakdown inlock down vs out of lockdown
all_hourly_obs  %>%
  mutate(date=as.Date(paste0(year,"-", month,"-",daynr)) ) %>% 
  mutate(in_lockdown = (date >=LOCKDOWN_FASEN$event_date[[1]] & date < LOCKDOWN_FASEN$event_date[[2]] )) %>%
  mutate(in_lockdown_desc = ifelse(in_lockdown, "In lockdown", "'No' lockdown")) %>%
  group_by(hour, in_lockdown_desc) %>%
  summarise(PM10 = mean(pm10, na.rm=T),
            PM25 = mean(pm25, na.rm=T)) %>%
  gather("Median", "Pollution", -hour, -in_lockdown_desc) %>%
  ggplot(aes(x=hour, y=Pollution, fill=Median) ) +
  geom_bar(stat='identity', position='dodge') +
  labs(x = "Hour", y = "mug/m3") +
  ggtitle( label = "Arnhem Centrum Air Quality (corrected data)",
           subtitle="avg hourly breakdown") +
  facet_wrap(~in_lockdown_desc) +
  theme_bw()



# via decompositie
daily_obs_xts <- as_xts(daily_obs$avg_pm10, by= daily_obs$date) 
daily_obs_comp <- decompose(daily_obs)
